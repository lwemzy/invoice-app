const getters = {
  allInvoices: state => state.invoices,
  invoiceById: state => state.invoice,
  loadedInvoice: state => !!state.invoices.length
};

export default getters;
